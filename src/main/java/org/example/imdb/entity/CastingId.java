package org.example.imdb.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CastingId implements Serializable {

    @Column(name = "movie_id")
    private Integer movieId;

    @Column(name = "actor_id")
    private Integer actorId;

    public CastingId() {
    }

    public CastingId(Integer movieId, Integer actorId) {
        this.movieId = movieId;
        this.actorId = actorId;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Integer getActorId() {
        return actorId;
    }

    public void setActorId(Integer actorId) {
        this.actorId = actorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CastingId castingId = (CastingId) o;
        return Objects.equals(movieId, castingId.movieId) && Objects.equals(actorId, castingId.actorId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(movieId, actorId);
    }

    @Override
    public String toString() {
        return "CastingId{" +
                "movieId=" + movieId +
                ", actorId=" + actorId +
                '}';
    }
}
