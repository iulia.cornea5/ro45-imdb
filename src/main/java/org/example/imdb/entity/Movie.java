package org.example.imdb.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Movie extends BaseEntity {


    private String name;

    @Column(name = "release_date")
    private Date releaseDate;

    // FetchType.EAGER e varianta default <- când încarcă o entiate Movie din baza de date
    // va aduce și obiectul de tipul Genre asociat ei
    @ManyToOne
    private Genre genre;

    // CascadeType.PERSIST <- când o entitate Movie fa vi persistată, va cascada (propaga) acțiunea PERSIST și către toate entitățile Tag din set (va încerca să le creeze și pe ele)
    // CascadeType.MERGE <- când o entitate Movie fa vi merge-uită, va cascade (propaga) acțiunea MERGE și către toate entitățile Tag din set (va încerca să le facă update sau create)
    // FetchType.EAGER <- când încarcă o entiate Movie din baza de date va aduce și obiectele de tipul Tag asociate ei

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinTable(name = "movie_tag", // numele tabelului de legătură între movie și tag (relațiile manyToMany se descompun în două relații oneToMany către tabelul de legătură)
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_name"))
    Set<Tag> tags = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "casting",
            joinColumns = @JoinColumn(name = "movie_id"),
            inverseJoinColumns = @JoinColumn(name = "actor_id")
    )
    private Set<Actor> actors = new HashSet<>();

    public Set<Actor> getActors() {
        return actors;
    }

    public void setActors(Set<Actor> actors) {
        this.actors = actors;
    }


    public Movie() {
    }

    public Movie(String name, Date releaseDate, Genre genre) {
        this.name = name;
        this.releaseDate = releaseDate;
        this.genre = genre;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }


    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public void addTag(Tag newTag) {
        this.tags.add(newTag);
    }

    public void removeTag(Tag toRemove) {
        this.tags.remove(toRemove);
    }


    @Override
    public String toString() {
        return "Movie{" +
                "id='" + this.getId() + '\'' +
                ", name='" + name + '\'' +
                ", releaseDate=" + releaseDate +
                ", genre=" + genre +
                ", tags=" + tags +
                '}';
    }
}
