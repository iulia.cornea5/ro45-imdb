package org.example.imdb.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Objects;

@Entity
public class Casting {

    @EmbeddedId
    private CastingId id;

    private Integer salary;

    public Casting() {
    }

    public Casting(CastingId id, Integer salary) {
        this.id = id;
        this.salary = salary;
    }

    public CastingId getId() {
        return id;
    }

    public void setId(CastingId id) {
        this.id = id;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Casting casting = (Casting) o;
        return Objects.equals(id, casting.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Casting{" +
                "id=" + id +
                ", salary=" + salary +
                '}';
    }
}
