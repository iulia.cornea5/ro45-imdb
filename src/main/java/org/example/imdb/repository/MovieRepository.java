package org.example.imdb.repository;

import org.example.imdb.entity.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.security.InvalidParameterException;
import java.util.List;

public class MovieRepository implements CRUDRepository<Movie> {

    private final SessionFactory sessionFactory;

    public MovieRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Movie create(Movie movie) {
        if (movie.getId() != null) {
            throw new InvalidParameterException("Movie create failed. Cannot create a movie which already has an id");
        }
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Movie savedMovie = (Movie) session.merge(movie);
        transaction.commit();
        session.close();
        return savedMovie;
    }

    public Movie update(Movie movie) {
        if (movie == null || movie.getId() == null) {
            throw new InvalidParameterException("Movie update failed. Cannot update a movie entity without id");
        }
        Session session = sessionFactory.openSession();
        if (session.find(Movie.class, movie.getId()) == null) {
            session.close();
            throw new InvalidParameterException("Movie update failed. Cannot find movie entity with id " + movie.getId() +
                    " in order to update it.");
        }
        Transaction transaction = session.beginTransaction();
        Movie savedMovie = (Movie) session.merge(movie);
        transaction.commit();
        session.close();
        return savedMovie;
    }

    public void remove(Integer movieId) {
        if(movieId == null) {
            throw new InvalidParameterException("Movie delete failed. Must specify a movie id.");
        }
        Session session = sessionFactory.openSession();
        Movie toDelete = session.find(Movie.class, movieId);
        if(toDelete == null) {
            session.close();
            throw new InvalidParameterException("Movie delete failed. There is no movie entity with given id " + movieId);
        }
        Transaction transaction = session.beginTransaction();
        session.remove(toDelete);
        transaction.commit();
        session.close();
    }


    @Override
    public List<Movie> findAll() {
        Session session = sessionFactory.openSession();
        List<Movie> movies = session.createQuery("from Movie", Movie.class).getResultList();
        session.close();
        return movies;
    }

    public Movie getMovieWithActors(Integer movieId) {
        Session session = sessionFactory.openSession();
        Movie m = session.createQuery(
                "select m from Movie m " +
                        " join fetch m.actors " +
                        " where m.id = :movieId", Movie.class)
                .setParameter("movieId", movieId)
                .getSingleResult();
        session.close();
        return m;
    }

}
