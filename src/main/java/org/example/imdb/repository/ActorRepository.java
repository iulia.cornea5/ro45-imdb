package org.example.imdb.repository;

import org.example.imdb.entity.Actor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ActorRepository implements CRUDRepository<Actor> {

    private final SessionFactory sessionFactory;

    public ActorRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Actor create(Actor actor) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(actor);
        transaction.commit();
        session.close();
        return actor;
    }

    @Override
    public List<Actor> findAll() {
        Session session = sessionFactory.openSession();
        List<Actor> actor = session.createQuery("from Actor", Actor.class).getResultList();
        session.close();
        return actor;
    }
}
