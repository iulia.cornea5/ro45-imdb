package org.example.imdb.repository;

import org.example.imdb.entity.Casting;
import org.example.imdb.entity.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public class CastingRepository {

    private final SessionFactory sessionFactory;

    public CastingRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Casting create(Casting casting) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.persist(casting);
        transaction.commit();
        session.close();
        return casting;
    }

    public Casting update(Casting casting) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Casting mergedCasting = (Casting) session.merge(casting);
        transaction.commit();
        session.close();
        return mergedCasting;
    }

    public List<Movie> getMoviesOfActor(Integer actorId) {
        Session session = sessionFactory.openSession();
        List<Movie> movies = session.createQuery(
                "select m from Movie m " +
                " join Casting c on c.id.movieId = m.id " +
                " join Actor a on c.id.actorId = a.id" +
                " where a.id = :actorId", Movie.class)
                .setParameter("actorId", actorId)
                .getResultList();
        session.close();
        return movies;
    }
}
