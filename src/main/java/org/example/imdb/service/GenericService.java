package org.example.imdb.service;

public interface GenericService<T> {

    T add();

    void displayAll();
}
