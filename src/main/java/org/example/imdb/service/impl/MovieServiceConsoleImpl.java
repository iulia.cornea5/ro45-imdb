package org.example.imdb.service.impl;

import org.example.imdb.entity.Genre;
import org.example.imdb.entity.Movie;
import org.example.imdb.entity.Tag;
import org.example.imdb.repository.GenreRepository;
import org.example.imdb.repository.MovieRepository;
import org.example.imdb.service.MovieService;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

public class MovieServiceConsoleImpl implements MovieService {

    private final MovieRepository movieRepository;
    private final GenreRepository genreRepository;

    public MovieServiceConsoleImpl(
            MovieRepository movieRepository,
            GenreRepository genreRepository) {
        this.movieRepository = movieRepository;
        this.genreRepository = genreRepository;
    }

    @Override
    public Movie read() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Numele filmului: ");
        String name = keyboard.nextLine();

        Date releaseDate = null;
        while (releaseDate == null) {
            System.out.println("Data lansării: ");
            String userInput = keyboard.nextLine();
            try {
                releaseDate = Date.valueOf(userInput);
            } catch (IllegalArgumentException e) {
                System.out.println("Ati introdus o data incorectă. Respectați formatul yyyy-MM-dd.");
            }
        }

        List<Genre> genreList = genreRepository.findAll();
        Genre genre = null;
        while (genre == null) {
            System.out.println("Introduceti unul dintre genurile: " +
                    genreList
                            .stream()
                            .map(g -> g.getName())
                            .collect(Collectors.toList()));
            String userInput = keyboard.nextLine().trim(); // șterge spațiile goale înainte sau după text 'horror ' -> 'horror'
            try {
                genre = genreList
                        .stream()
                        .filter(g -> g.getName().equalsIgnoreCase(userInput)) // Horror = horror
                        .findFirst()
                        .get();
            } catch (NoSuchElementException e) {
                System.out.println("Ați introdus un gen cinematic care nu se află în lista noastră. Respectați losta oferită.");
            }
        }

        System.out.println("Introduceti tag-uri pentru filmul " + name + " separate prin ';'");
        Movie movie = new Movie(name, releaseDate, genre);
        String allTags = keyboard.nextLine();
        Arrays.stream(allTags.split(";"))
                .map(t -> t.trim().toLowerCase())
                .forEach(t -> movie.addTag(new Tag(t)));

        return movie;
    }

    @Override
    public Movie add() {
        Movie newMovie = read();
        return movieRepository.create(newMovie);
    }

    @Override
    public void displayAll() {
        System.out.println(movieRepository.findAll());
    }
}
