package org.example.imdb.service.impl;

import org.example.imdb.entity.AppUser;
import org.example.imdb.entity.Movie;
import org.example.imdb.entity.Review;
import org.example.imdb.repository.AppUserRepository;
import org.example.imdb.repository.MovieRepository;
import org.example.imdb.repository.ReviewRepository;
import org.example.imdb.service.ReviewService;

import java.sql.Date;
import java.util.List;
import java.util.Scanner;

public class ReviewServiceConsoleImpl implements ReviewService {

    private final ReviewRepository reviewRepository;
    private final AppUserRepository appUserRepository;
    private final MovieRepository movieRepository;

    public ReviewServiceConsoleImpl(ReviewRepository reviewRepository, AppUserRepository appUserRepository, MovieRepository movieRepository) {
        this.reviewRepository = reviewRepository;
        this.appUserRepository = appUserRepository;
        this.movieRepository = movieRepository;
    }


    @Override
    public Review add() {
        return reviewRepository.create(read());
    }

    @Override
    public void displayAll() {
        System.out.println(reviewRepository.findAll());
    }

    private Review read() {
        Scanner keyboard = new Scanner(System.in);
        List<Movie> movieList = movieRepository.findAll();
        System.out.println("Scriți id-ul filmului pentru care vreți să lăsați un review: ");
        movieList.stream()
                .forEach(m -> System.out.println(m.getId() +
                        "\t" + m.getName() +
                        " din " + m.getReleaseDate()));
        Integer movieId = Integer.valueOf(keyboard.nextLine());

        Movie movie = movieList.stream().filter(m-> m.getId() == movieId).findFirst().get();
        System.out.println("Dati un rating pentru fimlul " + movie.getName());
        Integer rating = Integer.valueOf(keyboard.nextLine());

        System.out.println("Lasati un comentariu pentru nota " + rating);
        String content = keyboard.nextLine();
        AppUser appUser = appUserRepository.create(new AppUser("user@gmail.com", "123"));

        return new Review(appUser, rating, new Date(System.currentTimeMillis()), content, movie);
    }
}
