package org.example.imdb.service;

import org.example.imdb.entity.Movie;

public interface MovieService extends GenericService<Movie> {

    Movie read();
}
