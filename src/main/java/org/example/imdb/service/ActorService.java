package org.example.imdb.service;

import org.example.imdb.entity.Actor;

public interface ActorService extends GenericService<Actor> {
}
