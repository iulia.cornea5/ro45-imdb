package org.example.imdb.service;

import org.example.imdb.entity.Genre;
import org.example.imdb.entity.Movie;

public interface GenreService extends GenericService<Genre>  {

    Genre read();
}
