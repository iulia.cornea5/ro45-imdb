package org.example.imdb.service;

import org.example.imdb.entity.Review;

public interface ReviewService extends GenericService<Review> {
}
