package org.example.imdb.config;

import org.example.imdb.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateConfig {

    private static SessionFactory sessionFactory = null;

    public static SessionFactory getHibernateSessionFactory() {
        if(sessionFactory == null) {
            System.out.println("Creating session factory");
            sessionFactory = new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addAnnotatedClass(AppUser.class)
                    .addAnnotatedClass(Actor.class)
                    .addAnnotatedClass(Movie.class)
                    .addAnnotatedClass(Genre.class)
                    .addAnnotatedClass(Review.class)
                    .addAnnotatedClass(Tag.class)
                    .addAnnotatedClass(Casting.class)
                    .buildSessionFactory();
            System.out.println("Session factory created");
        }
        return sessionFactory;
    }

    public static void closeHibernateSessionFactory() {
        if(sessionFactory != null) {
            System.out.println("Closing session factory");
            sessionFactory.close();
            System.out.println("Session factory closed");
        }
    }
}
